#!/usr/local/bin/ruby

require 'net/imap'
require 'mail'
require 'fileutils'
require 'mimemagic'
require 'aws-sdk-s3'
require 'logger'

require 'dotenv/load' if File.exists?('.env')

if ENV['ENVIRONMENT'] == "kubernetes"
  LOG = Logger.new('/proc/1/fd/1')
else
  LOG = Logger.new(STDOUT)
end
LOG.level = ENV.fetch("LOG_LEVEL") { Logger::INFO }

class Storage

  attr_reader :s3_client

  def initialize
    @s3_client = Aws::S3::Client.new(
      access_key_id: ENV['AWS_ACCESS_KEY_ID'],
      secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
      endpoint: ENV['AWS_S3_ENDPOINT'],
      region: ENV['AWS_REGION']
    )
  end

  def upload(file_name)
    s3_client.put_object({
      bucket: ENV['AWS_S3_BUCKET'],
      key: "#{ENV['AWS_BUCKET_PATH']}/#{file_name.split(/\//).last}",
      body: File.read(file_name),
      acl: "private",
      content_encoding: "utf-8",
      content_type: MimeMagic.by_magic(File.open(file_name)).type
    })
  end
end


LOG.info("Started IMAP to S3 Bridge")

while true
  begin
    imap = Net::IMAP.new(ENV["IMAP_HOST"], ENV["IMAP_PORT"], ENV["IMAP_SSL"])
    imap.login(ENV["IMAP_USERNAME"], ENV["IMAP_PASSWORD"])

    target_mailboxes = imap.list("", ENV["IMAP_TARGET_FOLDER"])
    if target_mailboxes.nil?
      imap.create(ENV["IMAP_TARGET_FOLDER"])
    end

    imap.select(ENV["IMAP_SOURCE_FOLDER"])

    FileUtils.mkdir_p(ENV["PROCESSING_FOLDER"])

    imap.search('ALL').take(1).each do |message_id|
      result = imap.fetch(message_id, ['UID','RFC822'])
      if !result.nil?
        imap_message = result[0]
        body = imap_message.attr['RFC822']
        uid = imap_message.attr['UID']

        LOG.info("Received message #{uid}")

        mail = Mail.new(body)
        unless mail.attachments.empty?
          mail.attachments.each do |attachment|
            pattern = /#{ENV.fetch('PROCESSING_FILENAME_PATTERN') {".*"}}/
            file_name = attachment.filename.scan(pattern).last
            file_path = "#{ENV["PROCESSING_FOLDER"]}/#{file_name}"

            LOG.info("Upload attachment #{attachment.filename} as #{file_name}")

            File.open(file_path, 'wb') do |file|
              file.write(attachment.body.decoded)
            end

            Storage.new.upload(file_path)

            File.delete(file_path)
          end
        end
        imap.move(message_id, ENV.fetch("IMAP_TRASH_FOLDER") { "[Gmail]/Trash" })

        LOG.info("Archived message #{uid}")
      end
    end

    imap.logout
    imap.disconnect

  rescue => e
    LOG.error(([e.message] + e.backtrace).join("\n  "))
  ensure
    sleep ENV.fetch("IMAP_SLEEP") { "10" }.to_i
  end
end
